<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\db\ActiveRecord;
use app\models\Fotos;// acceso directo al modelo de fotos
use app\models\Paginas;

class SiteController extends Controller
{
    

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex(){
        /* ejecutar la consulta con activeRecord y devuelve un array de modelos*/
        //        $fotos = Fotos::findAll(["portada"=>1]);
        
        
        /* ejecutar la consulta con activeRecord y devuelve un ActiveQueryInterface */
        $fotos1= Fotos::find()// esto solo nos brinda un interface de ActiveRecords
                ->where(["portada"=>1])// esta es la condicion
                ->all();
        
        return $this->render('index',[
            "datos"=>$fotos1,
        ]);
    }
    
    public function actionDonde(){
        $texto= Paginas::find()
                ->where(["nombre"=>"donde estamos"])
                ->one();
        
        return $this->render('dondeestamos',[
                "texto"=>$texto,
        ]);
    }
    
    public function actionPagina($p){
       if($p==1){
           $texto= Paginas::find()
                   ->where(["nombre"=>"pagina1"])
                   ->one();
           
            return $this->render('paginas',[
            'datos'=>$p,
            'texto'=>$texto,   
            
        ]);
       }elseif($p==2){
           
           $texto= Paginas::find()
                   ->where(["nombre"=>"pagina2"])
                   ->one();
           
            return $this->render('paginas',[
            'datos'=>$p,
            'texto'=>$texto, 
            
        ]);
       }else{
           
           $texto= Paginas::find()
                   ->where(["nombre"=>"pagina 3"])
                   ->one();
           
            return $this->render('paginas',[
            'datos'=>$p,
            'texto'=>$texto,
            
        ]);
       }
        
        
    }
    public function actionFotos(){
        /* ejecutar la consulta con activeRecord y devuelve un array de modelos*/
        //        $fotos = Fotos::findAll(["portada"=>1]);
        
        
        /* ejecutar la consulta con activeRecord y devuelve un ActiveQueryInterface */
        $fotos1= Fotos::find()// esto solo nos brinda un interface de ActiveRecords
                ->where(["portada"=>0])// esta es la condicion
                ->all();
        
        return $this->render('index',[
            "datos"=>$fotos1,
        ]);
    }
}
