<?php
$this->title = 'Inicio';
?>
<div class="site-index">
    <div class="row">
        <?php 
            foreach($datos as $registro){
        ?>
        <div class="col-xs-12 col-md-6">
            <?= yii\helpers\Html::img("@web/imgs/$registro->nombre",[
                'class'=>'img-responsive img-rounded',
                'style'=>'margin-top:10px',
            ])?>   
            
        </div>
        <?php 
            }
        ?>
    </div>
</div>
